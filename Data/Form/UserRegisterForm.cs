using System.ComponentModel.DataAnnotations;

namespace Nep_Cloud_Storage.Form
{
    public class UserRegisterForm
    {
        [Required(ErrorMessage = "Verplicht")] // user needs a name
        [StringLength(32, MinimumLength = 1, ErrorMessage = "Naam is te lang.")] // in the database we only have 32 characters of space for the name
        public string Name {get; set;}
        [Required(ErrorMessage = "Verplicht")] // user needs a Password
        [DataType(DataType.Password)] // it is a password
        public string Password1 {get; set;}
        [Required(ErrorMessage = "Verplicht")] // we need to compare the Password
        [Compare("Password1", ErrorMessage = "Het wachtwoord en herhaalde wachtwoord zijn niet hetzelfde.")] // used to compare password2 with password1
        public string Password2 {get; set;}
    }
}