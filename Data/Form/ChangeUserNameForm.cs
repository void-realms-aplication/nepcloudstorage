using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;

namespace Nep_Cloud_Storage.Form
{
    public class ChangeUserNameForm
    {
        [Required] // we need it to login
        [StringLength(32, ErrorMessage = "Naam is te lang.")] // in the database we only have 32 characters of space for the name
        public string Name {get; set;}
    }
}