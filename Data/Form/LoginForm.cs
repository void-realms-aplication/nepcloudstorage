using System.ComponentModel.DataAnnotations;

namespace Nep_Cloud_Storage.Form
{
    public class LoginForm
    {
        [Required(ErrorMessage = "Verplicht")] // we need it to login
        [StringLength(32, MinimumLength = 1, ErrorMessage = "Naam is te lang.")] // in the database we only have 32 characters of space for the name
        public string Name {get; set;}
        [Required(ErrorMessage = "Verplicht")] // we need it to login
        [DataType(DataType.Password)] // it is a password
        public string Password {get; set;}
    }
}