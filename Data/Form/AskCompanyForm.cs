using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;

namespace Nep_Cloud_Storage.Form
{
    public class AskCompanyForm
    {
        [Required]
        // in the database we only have 32 characters of space for the companyCode
        [StringLength(32, ErrorMessage = "Bedrijfnaam is te lang.")]
        public string Company {get; set;}
    }
}