using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;

namespace Nep_Cloud_Storage.Form
{
    public class ChangePasswordForm
    {
        [Required] // we need it to login
        [DataType(DataType.Password)] // it is the current password
        public string CurrentPassword {get; set;}

        [Required] // we need it to login
        [DataType(DataType.Password)] // it is the new password
        public string NewPassword {get; set;}

        [Required] // we need it to login
        [DataType(DataType.Password)] // it is the new password repeated
        [Compare("NewPassword", ErrorMessage = "Het wachtwoord en herhaald wachtwoord zijn niet hetzelfde.")]
        public string RepeatPassword {get; set;}
    }
}