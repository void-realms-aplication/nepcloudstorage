using System;
using System.Security.Cryptography;
using System.Text;

namespace Nep_Cloud_Storage.Data
{
    public class Hash
    {
        /// <summary>
        /// turns a string into a hash
        /// </summary>
        /// <param name="hashme">expects a string that you want tot hash</param>
        public string Hasher(string hashme)
        {
            SHA256 sHA256 = SHA256.Create();
            byte[] hasmeHashed = sHA256.ComputeHash(Encoding.UTF8.GetBytes(hashme));
            return BitConverter.ToString(hasmeHashed).Replace("-", "");
        }
    }
}