using System;
using System.Timers;
using Nep_Cloud_Storage.Controller;
using Microsoft.Extensions.Configuration;
namespace Nep_Cloud_Storage.Data
{
    public class Chat
    {
        private static Timer aTimer;
        public int Counter{get; protected set;}
        public string Message{get; protected set;}
        public int[] Times{get; set;}
        public int AlarmTimer{get; protected set;}
        public bool MessageSend {get; protected set;}
        private ChatController chatController;

        /// <summary>
        /// Chat object can be created with database configuration.
        /// </summary>
        /// <param name="configuration">expects configuration to make connection with the database</param>
        public Chat(IConfiguration configuration)
        {
            chatController = new ChatController(configuration);
            chatController.GetAllMessages();
            Times = new int[chatController.TimeOfArrivalOfAllMessages.Length];

            // Sets the array 'Times' with the time of arrival from all messages.
            for (int i = 0; i < chatController.TimeOfArrivalOfAllMessages.Length; i++)
            {
                Times[i] = chatController.TimeOfArrivalOfAllMessages[i];
            }

            Counter = 0;
            SetTimer();
            MessageSend = false;
        }

        /// <summary>
        /// Chat object can be created without database configuration.
        /// </summary>
        public Chat(){}

        /// <summary>
        /// Sets the timer for when the first message will be send.
        /// Timer will reset automatic for the next message.
        /// </summary>
        public void SetTimer()
        {
            SetTimerInterval();
            // Timer for the first message. Will be set with the AlarmTimer as interval time.
            aTimer = new Timer(AlarmTimer);

            // When the timer is finished the elapsed event will be set. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        /// <summary>
        /// This is the method that will be called when the timer is finised.
        /// Here the method to send the message will be called.
        /// And the next timer will be set with the calculated interval for the next message. 
        /// </summary>
        /// <param name="configuration">expects configuration to make connection with the database</param>
        public void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            SendMessage();
            Counter++;
            SetTimerInterval();
            aTimer.Interval = AlarmTimer;
        }

        /// <summary>
        /// Sets the interval for the next timer.
        /// </summary>
        public void SetTimerInterval()
        {
            // If the message needs to be send directly the AlarmTime will be 1 to avoid any errors.
            // If the message take one or more minutes the AlarmTime will be calculated.
            if(Times[0] == 0)
            {
                AlarmTimer = 1;
            }
            else
            {
                //Calculates time for when the message needs to be send using the amount of minuets * 60000(= 1 minute).
                AlarmTimer = Times[0] * 60000;
            }
        }

        /// <summary>
        /// Gets message from the database.
        /// Sets boolean to true so the message can be shown.
        /// </summary>
        public virtual void SendMessage()
        {
            Message = chatController.GetMessage(Counter, "text");
            MessageSend = true;
            //Todo: front-end check if MessageSend = true to show the message and pop-up
            //Todo: set MessageSend to false after ^^^
            //Todo: show Popup (check with MessageSend?; Front-end?)
        }
    }
}
