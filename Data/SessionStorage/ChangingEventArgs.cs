namespace Nep_Cloud_Storage.Session
{
    public class ChangingEventArgs : ChangedEventArgs
    {
        public bool Cancel {get; set;}
    }
}