using Microsoft.Extensions.DependencyInjection;

namespace Nep_Cloud_Storage.Session
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddBlazoredSessionStorage(this IServiceCollection service)
        {
            return service.AddScoped<ISessionStorageService, SessionStorageService>()
                          .AddScoped<ISyncSessionStorageService1, SessionStorageService>();
        }
    }
}