using Microsoft.JSInterop;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace Nep_Cloud_Storage.Session
{
    public class SessionStorageService : ISessionStorageService, ISyncSessionStorageService1
    {
        private readonly IJSRuntime _jSRuntime;
        private readonly IJSInProcessRuntime _jSInProcessRuntime;

        ///<summary>
        /// fills _jSRuntime with jSRuntime
        /// fills _jSInProcessRuntime with jSRuntime as IJSInProcessRuntime
        ///</summary>
        /// <param name="jSRuntime">expects a instance of a JavaScript runtime</param>
        public SessionStorageService(IJSRuntime jSRuntime)
        {
            _jSRuntime = jSRuntime;
            _jSInProcessRuntime = jSRuntime as IJSInProcessRuntime;
        }

        public async ValueTask SetItemAsync<T>(string key, T data)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            ChangingEventArgs eventArgs = await RaiseOnChangingAsync(key, data).ConfigureAwait(false);

            // cancels this function
            if (eventArgs.Cancel) return;

            // if the data is a string it can directly be put in the storage slot
            // otherwise the data first needs to be serialised
            if (data is string)
            {
                await _jSRuntime.InvokeVoidAsync("sessionStorage.setItem", key, data).ConfigureAwait(false);
            }
            else
            {
                string serialisedData = JsonSerializer.Serialize(data);
                await _jSRuntime.InvokeVoidAsync("sessionStorage.setItem", key, serialisedData).ConfigureAwait(false);
            }

            RaiseOnChanged(key, eventArgs.OldValue, data);
        }

        public async ValueTask<T> GetItemAsync<T>(string key)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            string serialisedData = await _jSRuntime.InvokeAsync<string>("sessionStorage.getItem", key).ConfigureAwait(false);

            // if the string is null or whitespace then there is no data to return
            if (string.IsNullOrWhiteSpace(serialisedData))
                return default;

            // if the data is serialised it first needs to deserialize it before it returns the data
            if (serialisedData.StartsWith("{") && serialisedData.EndsWith("}")
                || serialisedData.StartsWith("\"") && serialisedData.EndsWith("\"")
                || typeof(T) != typeof(string))
            {
                return JsonSerializer.Deserialize<T>(serialisedData);
            }
            else
            {
                return (T)(object)serialisedData;
            }
        }

        public async ValueTask<string> GetItemAsStringAsync(string key)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            return await _jSRuntime.InvokeAsync<string>("sessionStorage.getItem", key).ConfigureAwait(false);
        }

        public async ValueTask RemoveItemAsync(string key)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            await _jSRuntime.InvokeVoidAsync("sessionStorage.removeItem", key).ConfigureAwait(false);
        }

        public async ValueTask ClearAsync() => await _jSRuntime.InvokeVoidAsync("sessionStorage.clear").ConfigureAwait(false);

        public async ValueTask<int> LengthAsync() => await _jSRuntime.InvokeAsync<int>("eval", "sessionStorage.length").ConfigureAwait(false);

        public async ValueTask<string> KeyAsync(int index) => await _jSRuntime.InvokeAsync<string>("sessionStorage.key", index).ConfigureAwait(false);

        public async ValueTask<bool> ContainKeyAsync(string key) => await _jSRuntime.InvokeAsync<bool>("sessionStorage.hasOwnProperty", key).ConfigureAwait(false);

        public void SetItem<T>(string key, T data)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            ChangingEventArgs eventArgs = RaiseOnChangingSync(key, data);

            // cancels this function
            if (eventArgs.Cancel)
                return;

            // if the data is a string it can directly be put in the storage slot
            // otherwise the data first needs to be serialised
            if (data is string)
            {
                _jSInProcessRuntime.InvokeVoid("sessionStorage.setItem", key, data);
            }
            else
            {
                _jSInProcessRuntime.InvokeVoid("sessionStorage.setItem", key, JsonSerializer.Serialize(data));
            }

            RaiseOnChanged(key, eventArgs.OldValue, data);
        }

        public T GetItem<T>(string key)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            string serialisedData = _jSInProcessRuntime.Invoke<string>("sessionStorage.getItem", key);

            // if the string is null or whitespace then there is no data to return
            if (string.IsNullOrWhiteSpace(serialisedData))
                return default;

            // if the data is serialised it first needs to deserialize it before it returns the data
            if (serialisedData.StartsWith("{") && serialisedData.EndsWith("}")
                || serialisedData.StartsWith("\"") && serialisedData.EndsWith("\"")
                || typeof(T) != typeof(string))
            {
                return JsonSerializer.Deserialize<T>(serialisedData);
            }
            else
            {
                return (T)(object)serialisedData;
            }
        }
        
        public string GetItemAsString(string key)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            return _jSInProcessRuntime.Invoke<string>("sessionStorage.getItem", key);
        }

        public void RemoveItem(string key)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            _jSInProcessRuntime.InvokeVoid("sessionStorage.removeItem", key);
        }

        public void Clear()
        {
            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            _jSInProcessRuntime.InvokeVoid("sessionStorage.clear");
        }

        public int Length()
        {
            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            return _jSInProcessRuntime.Invoke<int>("eval", "sessionStorage.length");
        }

        public string Key(int index)
        {
            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            return _jSInProcessRuntime.Invoke<string>("sessionStorage.key", index);
        }

        public bool ContainKey(string key)
        {
            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            return _jSInProcessRuntime.Invoke<bool>("sessionStorage.hasOwnProperty", key);
        }

        public event EventHandler<ChangingEventArgs> Changing;
        private async Task<ChangingEventArgs> RaiseOnChangingAsync(string key, object data)
        {
            ChangingEventArgs eventArgs = new ChangingEventArgs
            {
                Key = key,
                OldValue = await GetItemInternalAsync<object>(key).ConfigureAwait(false),
                NewValue = data
            };

            Changing?.Invoke(this, eventArgs);

            return eventArgs;
        }

        private ChangingEventArgs RaiseOnChangingSync(string key, object data)
        {
            ChangingEventArgs eventArgs = new ChangingEventArgs
            {
                Key = key,
                OldValue = GetItemInternal(key),
                NewValue = data
            };

            Changing?.Invoke(this, eventArgs);

            return eventArgs;
        }

        private async Task<T> GetItemInternalAsync<T>(string key)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            string serialisedData = await _jSRuntime.InvokeAsync<string>("sessionStorage.getItem", key).ConfigureAwait(false);

            // if the string is null or whitespace then there is no data to return
            if (string.IsNullOrWhiteSpace(serialisedData))
                return default;

            // if the data is serialised it first needs to deserialize it before it returns the data
            if (serialisedData.StartsWith("{") && serialisedData.EndsWith("}")
                || serialisedData.StartsWith("\"") && serialisedData.EndsWith("\""))
            {
                return JsonSerializer.Deserialize<T>(serialisedData);
            }
            else
            {
                return (T)(object)serialisedData;
            }
        }

        private object GetItemInternal(string key)
        {
            // there cannot be data in a storage slot without a name
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            // checks if _jSInProcessRuntime is available
            if (_jSInProcessRuntime == null)
                throw new InvalidOperationException("IJSInProcessRuntime not available");

            string serialisedData = _jSInProcessRuntime.Invoke<string>("sessionStorage.getItem", key);

            // if the string is null or whitespace then there is no data to return
            if (string.IsNullOrWhiteSpace(serialisedData))
                return default;

            // if the data is serialised it first needs to deserialize it before it returns the data
            if (serialisedData.StartsWith("{") && serialisedData.EndsWith("}")
                || serialisedData.StartsWith("\"") && serialisedData.EndsWith("\""))
            {
                try
                {
                    //Try to deserialize
                    return JsonSerializer.Deserialize<object>(serialisedData);
                }
                catch (JsonException)
                {
                    return serialisedData;
                }
            }
            else
            {
                return serialisedData;
            }
        }

        public event EventHandler<ChangedEventArgs> Changed;
        private void RaiseOnChanged(string key, object oldValue, object data)
        {
            ChangedEventArgs eventArgs = new ChangedEventArgs
            {
                Key = key,
                OldValue = oldValue,
                NewValue = data
            };

            Changed?.Invoke(this, eventArgs);
        }
    }
}
