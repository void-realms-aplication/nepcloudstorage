using System;
using System.Text;

namespace Nep_Cloud_Storage.Data
{
    public class Password
    {
        public int PasswordStrengthLevel{ get; private set;}
        public int PasswordLength{ get; private set;}
        public int PasswordAmountOfNumbers{ get; private set;}
        public int PasswordAmountOfSpecialCharacters{ get; private set;}
        public double PasswordEntropyLevel{ get; private set;}
        public int PasswordAmountUpperCase{ get; private set;}
        public int PasswordAmountLowerCase{ get; private set;}
        public int UniqueCharacters{ get; private set;}
    
        /// <summary>
        /// Password object can be created
        /// </summary>
        public Password()
        {
            PasswordStrengthLevel = 0;
            PasswordLength = 0;
            PasswordAmountOfNumbers = 0;
            PasswordAmountOfSpecialCharacters = 0;
            PasswordEntropyLevel = 0;
            PasswordAmountUpperCase = 0;
            PasswordAmountLowerCase = 0;
            UniqueCharacters = 0;
        }
        
        /// <summary>
        /// Password object can be created with multiply variables for testing
        /// </summary>
        public Password(int length, int amountNumbers, int amountSpecialChar, int amountUpperCase, int amountLowerCase, double entropy)
        {
            PasswordStrengthLevel = 0;
            PasswordLength = length;
            PasswordAmountOfNumbers = amountNumbers;
            PasswordAmountOfSpecialCharacters = amountSpecialChar;
            PasswordEntropyLevel = entropy;
            PasswordAmountUpperCase = amountUpperCase;
            PasswordAmountLowerCase = amountLowerCase;
            UniqueCharacters = 0;
        }

        /// <summary>
        /// Checks the strength of existing password
        /// </summary>
        public void CheckPasswordStrength(string password)
        {
            UniqueCharacters = 0;
            //Length of passwords get stored
            PasswordLength = password.Length;

            CheckCharactersAmount(password);
            CheckEntropyLevel();
            CheckPasswordLevelOfStrength();
        }

        /// <summary>
        /// Checks the strength level of password using a maturity model for password strength
        /// </summary>
        public void CheckPasswordLevelOfStrength()
        {
            //Default maturity level, lowest password strength
            int maturityLevel = 1;

            //Checks the maturity level of password
            if(PasswordLength >= 8 && PasswordEntropyLevel >= 28 && (PasswordAmountOfSpecialCharacters > 0 || PasswordAmountOfNumbers > 0))
            {
                maturityLevel++;
                if(PasswordLength >= 10 && PasswordEntropyLevel >= 35 && ( PasswordAmountUpperCase > 0 && PasswordAmountLowerCase > 0))
                {
                    maturityLevel++;
                    if(PasswordLength >= 12 && PasswordEntropyLevel >= 60)
                    {
                        maturityLevel++;
                        if(PasswordLength >= 15 && PasswordEntropyLevel >= 128 && PasswordAmountOfSpecialCharacters > 1)
                        {
                            maturityLevel++;
                        }
                    }
                }
            }
            PasswordStrengthLevel = maturityLevel;
        }

        /// <summary>
        /// Counts the amount of characters for given catagory using the ASCII range
        /// </summary>
        public void CheckCharactersAmount(string password)
        {
            Encoding ascii = Encoding.ASCII;
            byte[] bytes = new byte[password.Length];

            //Translates the characters in password to the numbers of the ACII table
            bytes = ascii.GetBytes(password);
            int unicode;

            for(int i = 0; i < bytes.Length; i++)
            {
                unicode = Convert.ToInt32(bytes[i]);
                //Checks if unicode is within the range for uppercases
                if (unicode >= 65 && unicode <= 90)
                {
                    PasswordAmountUpperCase++;
                }
                //Checks if unicode is within the range for lowercases
                if (unicode >= 97 && unicode <= 122)
                {
                    PasswordAmountLowerCase++;
                }
                //Checks if unicode is within the range for numbers
                if (unicode >= 48 && unicode <= 57)
                {
                    PasswordAmountOfNumbers++;
                }
                //Checks if unicode is within the range for Special characters
                if ((unicode >= 33 && unicode <= 47) || (unicode >= 58 && unicode <= 64) || (unicode >= 91 && unicode <= 96) || (unicode >= 123 && unicode <= 126))
                {
                    PasswordAmountOfSpecialCharacters++;
                }
            }
        }

        /// <summary>
        /// Checks entropy level of password
        /// Entropy level is based of general entropy rules for passwords
        /// Entropy is NOT base of Diceware Words List or any dictionary for simplicity
        /// </summary>
        public void CheckEntropyLevel()
        {
            CheckPossibleAmountOfUniqueCharacters();
            //Calculates entropy level
            PasswordEntropyLevel = Math.Log2(Math.Pow(UniqueCharacters, PasswordLength));           
        }

        /// <summary>
        /// Counts the amount of possible unique characters by checking if certain pools of characters are used
        /// _uniqueCharcters are based of the amount of possible characters the password can have. This is the same amount as the in the corrosponding array's.
        /// </summary>
        public void CheckPossibleAmountOfUniqueCharacters()
        {
           if(PasswordAmountOfSpecialCharacters > 0)
           {
                UniqueCharacters += 32;
           }
           if(PasswordAmountOfNumbers > 0)
           {
                UniqueCharacters += 10;
           }
           if(PasswordAmountUpperCase > 0)
           {
                UniqueCharacters += 26;
           }
           if(PasswordAmountLowerCase > 0)
           {
                UniqueCharacters += 26;
           }
        }
    }
}