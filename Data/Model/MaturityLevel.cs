namespace Nep_Cloud_Storage.Model
{
    /// <summary>
    /// holds all the data of a MaturityLevel
    /// </summary>
    public class MaturityLevel
    {
        int? Id {get; set;}
        int? PasswordStrengtLevel {get; set;}
        int? PopUpLevel {get; set;}

    }
}