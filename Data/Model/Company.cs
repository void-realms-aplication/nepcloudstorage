namespace Nep_Cloud_Storage.Model
{
    /// <summary>
    /// holds all the data of a Company
    /// </summary>
    public class Company
    {
        public string CompanyCode {get; set;}
        public string Password {get; set;}

        /// <summary>
        /// returns true if CompanyCode is null or empty
        /// </summary>
        public bool isEmpty()
        {
            return string.IsNullOrEmpty(CompanyCode);
        }
    }
}