using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nep_Cloud_Storage.Model
{
    public class Statistics
    {
        public int? Id {get; set;}

        public int? PasswordStrengthLevel {get; set;}

        public int? Length {get; set;}

        public int? AmountUpperCase {get; set;}

        public int? AmountLowerCase {get; set;}

        public int? AmountNumbers {get; set;}

        public int? AmountSpecialSymbols {get; set;}

        public double? Entropy {get; set;}

        public int? PopUpLevel {get; set;}

        public int? PopUpsClosed {get; set;}

        public int? PopUpsDeclined {get; set;}

        public int? PopUpsAccepted {get; set;}
    }
}