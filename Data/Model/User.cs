namespace Nep_Cloud_Storage.Model
{
    /// <summary>
    /// holds all the data of an User
    /// </summary>
    public class User
    {
        public User(){}
        public string Name {get; set;}
        public string Password {get; set;}
        public string CompanyCode {get; set;}
        public UserRoles Role {get; set;}
        public int? Id {get; set;}

        /// <summary>
        /// returns true if Name and Id ar null or empty
        /// </summary>
        public bool isEmpty()
        {
            return (string.IsNullOrEmpty(Name) && Id.Equals(null));
        }
    }
    public enum UserRoles
    {
        User,
        Admin
    }
}