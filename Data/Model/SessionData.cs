using System;

namespace Nep_Cloud_Storage.Model
{
    /// <summary>
    /// holds all the data of a SessionData
    /// </summary>
    public class SessionData
    {
        public string SessionKey {get; set;}
        public bool ForAnUser {get; set;}
        public int? UserId {get; set;}
        public string CompanyCode {get; set;}
        public DateTime DataTimeStartSession {get; set;}
    }
}