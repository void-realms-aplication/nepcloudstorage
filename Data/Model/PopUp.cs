using Microsoft.Extensions.Configuration;

namespace Nep_Cloud_Storage.Model
{
    /// <summary>
    /// holds all the data of a Company
    /// </summary>
    public class PopUp
    {
        public int Id {get; set;}
        public string Text {get; set;}
        public int DifficultyLevel {get; set;}
        public string Style {get; set;}
        public int TimeOfArrival {get; set;}
    }
}