using System;

namespace Nep_Cloud_Storage.Model
{
    /// <summary>
    /// holds all the data of a File
    /// </summary>
    public class FileData
    {
        public int? Id {get; set;}
        public string Document {get; set;}
        public DateTime Date {get; set;}
        public string Size {get; set;}
    }
}