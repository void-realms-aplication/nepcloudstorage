using System;
using Microsoft.Extensions.Configuration;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.Form;
using Nep_Cloud_Storage.DataBaseConnection;

namespace Nep_Cloud_Storage.Interface
{
    public interface ControllerInterface<T> where T : class {
           
        void Create();
        void Login<T>(string name, string password);
        bool ChecksAccountExist<T>(T t);
        void GetinProperty<T>(bool onlyNameOrId);
    }   
}