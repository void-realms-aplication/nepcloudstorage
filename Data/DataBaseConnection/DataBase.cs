using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using Nep_Cloud_Storage.Model;
using System.Collections.Generic;
using System.Linq;

namespace Nep_Cloud_Storage.DataBaseConnection
{
    public class DataBase
    {
        [Flags]
        public enum ReturnTypeValue 
        {
            StringValue = 1,
            IntValue = 2,
            IntArrayValue = 3,
            BoolValue = 4,
            UserValue = 5,
            CompanyValue = 6,
            PopUpValue = 7,
            StatisticsValue = 8
        }
        DataBaseAccess dataBaseAccess;
        MySqlDataReader result;
        Dictionary<ReturnTypeValue, Action> TypeValues;
        int resultNum;
        dynamic returnValue;

        /// <summary>
        /// Gives access to the database
        /// Sets dictionary to call specific methods using the enum values of 'ReturnTypeValue'
        /// </summary>
        /// <param name="configuration">expects configuration data of the database</param>
        public DataBase(IConfiguration configuration)
        {
            dataBaseAccess = new DataBaseAccess(configuration);
            TypeValues = new Dictionary<ReturnTypeValue, Action>()
            {
                { ReturnTypeValue.StringValue, ()=> GetString() },
                { ReturnTypeValue.IntValue, ()=> GetInt() },
                { ReturnTypeValue.IntArrayValue, ()=> GetIntArray() },
                { ReturnTypeValue.BoolValue, ()=> GetBoolean() },
                { ReturnTypeValue.UserValue, ()=> GetUser() },
                { ReturnTypeValue.CompanyValue, ()=> GetCompany() },
                { ReturnTypeValue.PopUpValue, ()=> GetPopUp() },
                { ReturnTypeValue.StatisticsValue, ()=> GetStatistics() }
            };
        }

        /// <summary>
        /// Gives access to the database
        /// </summary>
        public DataBaseAccess sendDataBaseAccess()
        {
            return dataBaseAccess;
        }
        
        /// <summary>
        /// Sends SQL code to the database
        /// </summary>
        /// <param name="sQLCode">expects SQL code</param>
        public void SendSQLCode(string sQLCode)
        {
            MySqlConnection con = new MySqlConnection(dataBaseAccess.ConnectionString);
            con.Open();
            con.ChangeDatabase(dataBaseAccess.Database);
            MySqlCommand myCommand = new MySqlCommand(sQLCode, con);
            myCommand.ExecuteNonQuery();
            con.Close();
        }
        
        /// <summary>
        /// Sends SQL code to the database and sends (a) session(s) back
        /// In the form of an array
        /// </summary>
        /// <param name="sQLCode">expects SQL code</param>
        public SessionData[] SendSQLCodeWithSessionReturn(string sQLCode, bool ForAnUser)
        {
            MySqlConnection con = new MySqlConnection(dataBaseAccess.ConnectionString);
            con.Open();
            con.ChangeDatabase(dataBaseAccess.Database);
            MySqlCommand myCommand = new MySqlCommand(sQLCode, con);
            MySqlDataReader result = myCommand.ExecuteReader();

            List<SessionData> sessions = new List<SessionData>();
            while (result.Read())
            {
                /// Adds sessiondata for an user
                if (ForAnUser)
                {
                    sessions.Add(new SessionData { SessionKey = result.GetString(0), DataTimeStartSession = result.GetDateTime(1), UserId = result.GetInt32(2)});
                }
                /// Adds sessiondata for a company
                else
                {
                    sessions.Add(new SessionData { SessionKey = result.GetString(0), DataTimeStartSession = result.GetDateTime(1), CompanyCode = result.GetString(2)});
                }
            }
            con.Close();
            return sessions.ToArray();
        }

        /// <summary>
        /// Sends SQL code to the database and sends the right variable type(T) back
        /// </summary>
        /// <param name="sQLCode">expects SQL code</param>
        public T SendSQLCodeWithReturn<T>(string sQLCode, ReturnTypeValue type)
        {
            MySqlConnection con = new MySqlConnection(dataBaseAccess.ConnectionString);
            con.Open();
            con.ChangeDatabase(dataBaseAccess.Database);
            MySqlCommand myCommand = new MySqlCommand(sQLCode, con);
            result = myCommand.ExecuteReader();

            // Calls the correct method using given enum value in 'type'.
            TypeValues[type]();
            con.Close();
            return returnValue;
        }

        ///<summary>
        /// Gets a string from the given result.
        /// Puts the string in returnValue.
        ///</summary>
        public void GetString()
        {
            if(result.Read())
            {
                returnValue = result.GetString(0);
            }
        }

        ///<summary>
        /// Gets an int from the given result.
        /// Puts the int in returnValue.
        ///</summary>
        public void GetInt()
        {
            if(result.Read())
            {
                returnValue = result.GetInt32(0);
            }
        }

        ///<summary>
        /// Creates users with the values of the given result.
        /// Puts the first user in returnValue.
        ///</summary>
        public void GetUser()
        {
            List<User> users = new List<Model.User>();
            if(result.Read())
            {
                users.Add(new Model.User { Id = result.GetInt32(0), Name = result.GetString(1), Password = result.GetString(2), Role = Enum.Parse<UserRoles>(result.GetString(3)), CompanyCode = result.GetString(4)});
            }

            returnValue = (from Model.User U in users
            orderby U.Id
            select U).FirstOrDefault();
        }

        ///<summary>
        /// Creates companies with the values of the given result.
        /// Puts the first company in returnValue.
        ///</summary>
        public void GetCompany()
        {
            List<Company> companies = new List<Model.Company>();
            if(result.Read())
            {
                companies.Add(new Model.Company { CompanyCode = result.GetString(0), Password = result.GetString(1)});
            }

            returnValue = (from Model.Company C in companies
            orderby C.CompanyCode
            select C).FirstOrDefault();
        }

        ///<summary>
        /// Creates pop-ups with the values of the given result.
        /// Puts the pop-ups in returnValue as array.
        ///</summary>
        public void GetPopUp()
        {
            List<PopUp> popUps = new List<Model.PopUp>();
            if(result.Read())
            {
                popUps.Add(new Model.PopUp { Id = result.GetInt32(0), Text = result.GetString(0), DifficultyLevel= result.GetInt32(0), Style = result.GetString(0), TimeOfArrival = result.GetInt32(0)});
            }
            returnValue = popUps.ToArray();
        }  

        ///<summary>
        /// Gets a int from the values of the given result.
        /// Puts the int in a returnValue and converts it to a boolean.
        ///</summary>     
        public void GetBoolean()
        {
            resultNum = 0;
            while (result.Read())
            {
                resultNum = Convert.ToInt32(result.GetInt64(0));
            }
            returnValue = resultNum >= 1;
        }

        ///<summary>
        /// Gets integers from the values of the given result.
        /// puts the integers in returnValue as array.
        ///</summary>
        public void GetIntArray()
        {
            List<int> integers = new List<int>();
            while (result.Read())
            {
                integers.Add(result.GetInt32(0));
            }
            returnValue = integers.ToArray();
        }

        ///<summary>
        /// Creates Statistics with the values of the given result.
        /// Puts the Statistics in returnValue as array.
        ///</summary>
        public void GetStatistics()
        {
            List<Model.Statistics> statistics = new List<Statistics>();
            while (result.Read())
            {
                //Add the statistic results to the array 'statistics'
                //For each result, there is a test to see whether the cell in the database table is NULL. If so, the result is left blank in the array.
                statistics.Add(new Statistics {
                Id = result.IsDBNull(0)? null : result.GetInt32(0),
                PasswordStrengthLevel = result.IsDBNull(1)? null : result.GetInt32(1),
                //Adds the amount of uppercase, lowercase, numbers and special symbols up. The result is the length of the password.
                Length =
                    (result.IsDBNull(2)? null : result.GetInt32(2))
                    +
                    (result.IsDBNull(3)? null : result.GetInt32(3))
                    +
                    (result.IsDBNull(4)? null : result.GetInt32(4))
                    +
                    (result.IsDBNull(5)? null : result.GetInt32(5)),
                AmountUpperCase = result.IsDBNull(2)? null : result.GetInt32(2),
                AmountLowerCase = result.IsDBNull(3)? null : result.GetInt32(3),
                AmountNumbers = result.IsDBNull(4)? null : result.GetInt32(4),
                AmountSpecialSymbols = result.IsDBNull(5)? null : result.GetInt32(5),
                Entropy = result.IsDBNull(6)? null : result.GetDouble(6),
                PopUpLevel = result.IsDBNull(7)? null : result.GetInt32(7),
                PopUpsClosed = result.IsDBNull(8)? null : result.GetInt32(8),
                PopUpsDeclined = result.IsDBNull(9)? null : result.GetInt32(9),
                PopUpsAccepted = result.IsDBNull(10)? null : result.GetInt32(10)});
            }  
            returnValue = statistics.ToArray();
        }

        /// <summary>
        /// Sends SQL code to the database server.
        /// </summary>
        /// <param name="sQLCode">expects SQL code</param>
        public void SendSQLCodeToServer(string sQLCode)
        {
            MySqlConnection con = new MySqlConnection(dataBaseAccess.ConnectionString);
            con.Open();
            MySqlCommand myCommand = new MySqlCommand(sQLCode, con);
            myCommand.ExecuteNonQuery();
            con.Close();
        }
    }
}
