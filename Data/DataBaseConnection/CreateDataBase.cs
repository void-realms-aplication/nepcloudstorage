using Microsoft.Extensions.Configuration;

namespace Nep_Cloud_Storage.DataBaseConnection
{
    public class CreateDataBase
    {
        DataBase database;
        DataBaseAccess dataBaseAccess;

        /// <summary>
        /// Creates everything of the database
        /// </summary>
        /// <param name="Configuration">expects IConfiguration</param>
        public CreateDataBase(IConfiguration Configuration)
        {
            database = new DataBase(Configuration);
            dataBaseAccess = database.sendDataBaseAccess();

            MakeDataBase();
            MakeDataBaseTables();
        }
        
        /// <summary>
        /// Creates the database if it does not exist
        /// </summary>
        public void MakeDataBase()
        {
            database.SendSQLCodeToServer($"CREATE DATABASE  IF NOT EXISTS `{dataBaseAccess.Database}`");
        }
        
        /// <summary>
        /// Creates the database tables if it does not exist
        /// Creates stored procedure
        /// </summary>
        public void MakeDataBaseTables()
        {
            /// Creates tables
            database.SendSQLCode(
            "CREATE TABLE IF NOT EXISTS Company( " + 
            "companyCode varchar(32) not null, " + 
            "password varchar(64) not null, " + 
            "primary key (companyCode)); " + 

            "CREATE TABLE IF NOT EXISTS User( " + 
            "id int not null AUTO_INCREMENT, " + 
            "username varchar(32) not null, " + 
            "password varchar(64) not null, " + 
            "role ENUM('Admin', 'User') NOT NULL DEFAULT 'User', " + 
            "companyCode varchar(32) not null, " + 
            "primary key (id), " + 
            "foreign key (companyCode) references Company (companyCode) on delete cascade on update cascade); " + 

            "CREATE TABLE IF NOT EXISTS MaturityLevel( " + 
            "userId int not null AUTO_INCREMENT, " + 
            "passwordDifferenceLevel smallint, " + 
            "passwordStrengthLevel smallint," + 
            "reportIncidentsLevel smallint, " + 
            "downloadFilesLevel smallint, " + 
            "popupLevel smallint, " + 
            "primary key (userId), " + 
            "foreign key (userId) references User (Id) on delete cascade on update cascade); " + 

            "CREATE TABLE IF NOT EXISTS PasswordDifferenceData( " + 
            "id int not null AUTO_INCREMENT, " + 
            "continuedCounting bool not null, " + 
            "amountSameCharacters int not null, " + 
            "caseChange int not null, " + 
            "onlyChangeSymbols bool not null, " + 
            "onlyChangeNumbers bool not null, " + 
            "lengthDifference int not null, " + 
            "dateOfTest date not null, " + 
            "userId int not null, " + 
            "primary key (id), " + 
            "foreign key (userId) references User (id) on delete cascade on update cascade); " + 

            "CREATE TABLE IF NOT EXISTS PasswordStrengthData( " + 
            "id int not null AUTO_INCREMENT, " + 
            "amountUpperCases int not null, " + 
            "amountLowerCases int not null, " + 
            "amountNumbers int not null, " + 
            "amountSymbols int not null, " + 
            "entropy double(32, 28) not null, " + 
            "dateOfTest date not null, " + 
            "userId int not null, " + 
            "primary key (id), " + 
            "foreign key (userId) references User (id) on delete cascade on update cascade); " + 

            "CREATE TABLE IF NOT EXISTS ReportIncidentData( " + 
            "id int not null AUTO_INCREMENT, " + 
            "category int(1) not null, " + 
            "description varchar(255) not null, " + 
            "dateOfTest date not null, " + 
            "userId int not null, " + 
            "primary key (id), " + 
            "foreign key (userId) references User (id) on delete cascade on update cascade); " + 

            "CREATE TABLE IF NOT EXISTS DownloadFilesData( " + 
            "id int not null AUTO_INCREMENT, " + 
            "amountGoodFiles int not null, " + 
            "amountBadFiles int not null, " + 
            "amountBadExecuted int not null, " + 
            "dateOfTest date not null, " + 
            "userId int not null, " + 
            "primary key (id), " + 
            "foreign key (userId) references User (id) on delete cascade on update cascade); " + 

            " CREATE TABLE IF NOT EXISTS Popups( " + 
            "id int not null AUTO_INCREMENT, " + 
            "text varchar(255) not null, " + 
            "difficultyLevel int(2) not null, " + 
            "style varchar(15) not null," +
            "timeOfArrival int(2) not null, primary key (id));" + 

            "CREATE TABLE IF NOT EXISTS PopupData( " + 
            "id int not null AUTO_INCREMENT, " + 
            "reaction int(1) not null, " + 
            "dateOfTest date not null, " + 
            "userId int not null, " + 
            "popupId int, primary key (id), " + 
            "foreign key FK1 (userId) references User (id) on delete cascade on update cascade, " + 
            "foreign key FK2 (popupId) references Popups (id) on delete set null on update cascade); " + 
            
            "CREATE TABLE IF NOT EXISTS Session( " + 
            "sessionKey varchar(64) not null, " + 
            "forAnUser bool null, " + 
            "dateTimeEndSession datetime not null, " + 
            "userId int, " + 
            "companyCode varchar(32), " + 
            "primary key (sessionKey), " + 
            "foreign key (userId) references User (id) on delete cascade on update cascade, " + 
            "foreign key (companyCode) references company (companyCode) on delete cascade on update cascade); "
            );

            /// Creates procedure insert_company
            database.SendSQLCode(
            "DROP procedure IF EXISTS `insert_company`;" +
            $"CREATE DEFINER=`{dataBaseAccess.Username}`@`{dataBaseAccess.Server}` PROCEDURE `insert_company`(" + 
            "IN GivenCompanyCode VARCHAR(32), " + 
            "CompanyPassword varchar(64)) " + 
            " BEGIN " + 
            "INSERT INTO `company` VALUES (GivenCompanyCode, CompanyPassword); " + 
            "END "
            );

            /// Creates procedure check_company
            database.SendSQLCode(
            "DROP procedure IF EXISTS `check_company`;" +
            $"CREATE DEFINER=`{dataBaseAccess.Username}`@`{dataBaseAccess.Server}` PROCEDURE `check_company` (" + 
            "IN GivenCompanyCode VARCHAR(32), " +
            "CompanyPassword varchar(64)) " + 
            " BEGIN " + 
            "SELECT * FROM company " + 
            "WHERE companyCode like GivenCompanyCode and password like CompanyPassword; " + 
            "END "
            );

            /// Creates procedure insert_user
            database.SendSQLCode(
            "DROP procedure IF EXISTS `insert_user`;" +
            $"CREATE DEFINER=`{dataBaseAccess.Username}`@`{dataBaseAccess.Server}` PROCEDURE `insert_user` (" + 
            "IN username varchar(32), " + 
            "userPassword varchar(64), " + 
            "role ENUM('Admin', 'User'), " + 
            "companyCode varchar(32)) " + 
            " BEGIN " + 
            "INSERT INTO `user` (`username`, `password`, `role`, `companyCode`) VALUES (username, userPassword, role, companyCode); " + 
            "END "
            );

            /// Creates procedure check_user
            database.SendSQLCode(
            "DROP procedure IF EXISTS `check_user`;" +
            $"CREATE DEFINER=`{dataBaseAccess.Username}`@`{dataBaseAccess.Server}` PROCEDURE `check_user` (" + 
            "IN UName VARCHAR(32), " +
            "UPassword varchar(64)) " + 
            " BEGIN " + 
            "SELECT * FROM user " + 
            "WHERE username like UName and password like UPassword; " + 
            "END "
            );

            /// Creates event_scheduler e_hour
            database.SendSQLCode(
            "SET GLOBAL event_scheduler = ON; " +
            "create event IF NOT EXISTS e_hour " +
            "on Schedule every 1 hour " +
            "do DELETE FROM `session` WHERE (`dateTimeEndSession` > now());"
            );
        }
    }
}
