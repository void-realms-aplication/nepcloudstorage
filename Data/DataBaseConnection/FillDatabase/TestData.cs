using Microsoft.Extensions.Configuration;
using Nep_Cloud_Storage.Controller;
using Nep_Cloud_Storage.Model;

namespace Nep_Cloud_Storage.DataBaseConnection
{
    public class TestData
    {
        IConfiguration Configuration;
        TokenController tokenController;
        DataBase database;

        /// <summary>
        /// fills the database with companies and users
        /// </summary>
        /// <param name="configuration">expects configuration</param>
        public TestData(IConfiguration configuration)
        {
            Configuration = configuration;
            tokenController = new TokenController();
            AddCompany();
            AddUser();
            AddMessage();
        }

        /// <summary>
        /// Creates and adds multiple companies to the database
        /// </summary>
        public void AddCompany()
        {
            CompanyController companyController = new CompanyController(Configuration);
            companyController.SetAndCreate("testCompany1", tokenController.CreateHash("1234"), true);
            companyController.SetAndCreate("testCompany2", tokenController.CreateHash("password"), true);
            companyController.SetAndCreate("HBLTestTeam", tokenController.CreateHash("HBLTestTeam"), true);
            companyController.SetAndCreate("HBLTeam", tokenController.CreateHash("HBLTeam"), true);
        }

        /// <summary>
        /// Creates and adds multiple users to the database
        /// </summary>
        public void AddUser()
        {
            UserController userController = new UserController(Configuration);
            userController.SetAndCreate("zelle", tokenController.CreateHash("1234"), UserRoles.Admin, "HBLTeam", true);
            userController.SetAndCreate("testUser1", tokenController.CreateHash("1234"), UserRoles.User, "testCompany1", true);
            userController.SetAndCreate("testUser2", tokenController.CreateHash("password"), UserRoles.User, "testCompany1", true);
            userController.SetAndCreate("HBLTestuser1", tokenController.CreateHash("HBLTestTeam"), UserRoles.Admin, "HBLTestTeam", true);
            userController.SetAndCreate("HBLAdmin", tokenController.CreateHash("HBLTeam"), UserRoles.Admin, "HBLTeam", true);
        }
        
        /// <summary>
        /// Adds multiple message to the database.
        /// </summary>
        public void AddMessage()
        {
            database = new DataBase(Configuration);
            ChatController chatController = new ChatController(Configuration);
            chatController.CreateMessage(1, "iets", 1, "normal", 0);
            chatController.CreateMessage(2, "anders", 1, "normal", 1);
        }
    }
}