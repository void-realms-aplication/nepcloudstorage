using Microsoft.Extensions.Configuration;

namespace Nep_Cloud_Storage.DataBaseConnection
{
    public class DataBaseAccess
    {
        private IConfiguration _config;
        public string Database {get;}
        public string Server {get;}
        public string Username {get;}

        /// <summary>
        /// Sets database login to give acces to the database
        /// </summary>
        public DataBaseAccess(IConfiguration configuration)
        {
            _config = configuration;
            Database = _config.GetValue<string>("DbConfig:DatabaseName");
            Server = _config.GetValue<string>("DbConfig:ServerName");
            Username = _config.GetValue<string>("DbConfig:UserName");
        }
        
        /// <summary>
        /// Generates the database connection string
        /// </summary>
        public string ConnectionString
        {
            get
            {
                string server = _config.GetValue<string>("DbConfig:ServerName");
                string port = _config.GetValue<string>("DbConfig:Port");
                string username = _config.GetValue<string>("DbConfig:UserName");
                string password = _config.GetValue<string>("DbConfig:Password");

                return ($"Server={server};Port={port};User ID={username};Password={password};");
            }
        }
    }
}