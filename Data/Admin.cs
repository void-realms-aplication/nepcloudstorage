using System;
using System.Text;

namespace Nep_Cloud_Storage.Data
{
    public class Admin 
    {
        ///<summary>
        /// Creates an Admin object and an companyController object.
        ///</summary>
        public Admin(){}

        ///<summary>
        ///Generates a random password of 8 characters
        ///</summary>
        public string GeneratePassword()
        {
            Encoding ascii = Encoding.ASCII;
            byte[] bytes = new byte[8];
            Random random = new Random();

            //Generates 8 random bytes and puts them in the array bytes.
            for(int i = 0; i < bytes.Length; i++)
            {
                //Creates random byte. 
                //Min. and max. are the numbers from the ASCII table were the character is a readable character on your keyboard.
                bytes[i] = Convert.ToByte(random.Next(33, 126));
            }
            //Converts all bytes in the array bytes to one string and returns its value.
            return ascii.GetString(bytes);
        }
    }
}