using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Nep_Cloud_Storage.Controller;
using Nep_Cloud_Storage.DataBaseConnection;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.Session;

namespace Nep_Cloud_Storage.Authentication
{
    public class MyAuthenticationStateProvider : AuthenticationStateProvider
    {
        private ISessionStorageService _sessionStorage;
        private UserController _userController;
        private CompanyController _companyController;
        private SessionController _sessionController;
        private DataBase _DataBase;
        public MyAuthenticationStateProvider(ISessionStorageService sessionStorageService, IConfiguration configuration)
        {
            _sessionStorage = sessionStorageService;
            _userController = new UserController(configuration);
            _companyController = new CompanyController(configuration);
            _sessionController = new SessionController(configuration);
            _DataBase = new DataBase(configuration);
        }

        /// <summary>
        /// this code will be executed when the site indicates that something has changed
        /// so this will be executed when you reload the page or when you change the page
        /// </summary>

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            SessionData storageSessionData = await _sessionStorage.GetItemAsync<SessionData>("session");
            SessionData sessionData;

            string name;
            string password;
            UserRoles userRole = new UserRoles();

            AuthenticationState authenticationState = new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
            // checks if there is storageSessionData
            // if not there is no logged in user or company
            if (storageSessionData != null)
            {
                sessionData = _sessionController.GetSession(storageSessionData);
                // checks if it is an user or a company
                if (storageSessionData.ForAnUser)
                {
                    _userController.CId = storageSessionData.UserId;
                    _userController.GetinProperty<User>(true);
                    name = _userController.CUser.Name;
                    password = _userController.CUser.Password;
                    userRole = _userController.CUser.Role;
                }
                else
                {
                    _companyController.CCompanyCode = storageSessionData.CompanyCode;
                    _companyController.GetinProperty<Company>(true);
                    name = _companyController.CCompany.CompanyCode;
                    password = _companyController.CCompany.Password;
                }
                // checks if the session is valid
                if (_sessionController.CheckSession(sessionData, name, password))
                {
                    sessionData = _sessionController.UpdateSession(sessionData, name, password);
                    authenticationState = SetAuthenticationState(name, (sessionData.ForAnUser ? userRole.ToString() : "company"));
                }
            }
            return await Task.FromResult(authenticationState);
        }

        /// <summary>
        /// AuthUserWithRole will log you in
        /// </summary>
        private AuthenticationState SetAuthenticationState(string name, string role)
        {
            ClaimsIdentity identity = new ClaimsIdentity(new List<Claim>
            {
                new Claim(ClaimTypes.Name, name),
                new Claim(ClaimTypes.Role, role)
            }, "WebApiAuth");
            return new AuthenticationState(new ClaimsPrincipal(identity));
        }

        /// <summary>
        /// AuthUserWithRole will log you in
        /// </summary>
        public void AuthUserWithRole(string name, string role)
        {
            ClaimsIdentity identity = new ClaimsIdentity(new List<Claim>
            {
                new Claim(ClaimTypes.Name, name),
                new Claim(ClaimTypes.Role, role)
            }, "WebApiAuth");
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(new ClaimsPrincipal(identity))));
        }

        /// <summary>
        /// Logout will log you out
        /// </summary>
        public void Logout()
        {
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()))));
        }
    }
}