using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.DataBaseConnection;
using MySql.Data.MySqlClient;
namespace Nep_Cloud_Storage.Controller
{
    public class ChatController
    {
        DataBase database;
        PopUp popUp;
        public int[] TimeOfArrivalOfAllMessages{get; set;}

        /// <summary>
        /// Configuration is needed for sending querries to the database.
        /// </summary>
        /// <param name="configuration">expects configuration</param>
        public ChatController(IConfiguration configuration)
        {
            database = new DataBase(configuration);
            popUp = new PopUp();
        }

        /// <summary>
        /// Is needed for testing, this should not be used anywhere else.
        /// As the database cannot be contacted with it.
        /// </summary>
        public ChatController(){}

        /// <summary>
        /// Creates a message with the given id, text, difficulty level and style
        /// </summary>
        /// <param name="id">expects id</param>
        /// <param name="text">expects text</param>
        /// <param name="difficultylevel">expects difficultylevel</param>
        /// <param name="style">expects style</param>
        /// <param name="timeOfArrival">expects timeOfArrival</param>
        public virtual void CreateMessage(int id, string text, int difficultylevel, string style, int timeOfArrival)
        {
            popUp.Id = id;
            popUp.Text = text;
            popUp.DifficultyLevel = difficultylevel;
            popUp.Style = style;
            popUp.TimeOfArrival = timeOfArrival;
            SaveMessage(popUp);
        }

        /// <summary>
        /// Gets a message from the database that needs to be send on the chat.
        /// </summary>
        /// <param name="id">expects id</param>
        /// <param name="selection">expects colom name to select</param>
        public string GetMessage(int id, string selection)
        {
            return database.SendSQLCodeWithReturn<string>("SELECT {selection} FROM popups " + $"where id = '{id}';", DataBase.ReturnTypeValue.StringValue);
        }

        /// <summary>
        /// Gets the time of arrival of all existing messages from the database.
        /// </summary>
        public void GetAllMessages()
        {
            TimeOfArrivalOfAllMessages = database.SendSQLCodeWithReturn<int[]>("SELECT {timeOfArrival} FROM popups ;", DataBase.ReturnTypeValue.IntArrayValue);
        }
        
        /// <summary>
        /// Saves a given message as pop-up to the database.
        /// </summary>
        /// <param name="popUp">expects pop-up</param>
        public virtual void SaveMessage(PopUp popUp)
        {
            if(!CheckMessageExist(popUp.Id))
            {
                database.SendSQLCode($"INSERT INTO popups(id,text,difficultyLevel,style,timeOfArrival) VALUES ('{popUp.Id}', '{popUp.Text}', '{popUp.DifficultyLevel}', '{popUp.Style}', '{popUp.TimeOfArrival}');");
            }
        }

        /// <summary>
        /// Checks if message already exists.
        /// </summary>
        /// <param name="id">expects a pop-up id</param>
        public virtual bool CheckMessageExist(int id)
        {
            PopUp[] popups = database.SendSQLCodeWithReturn<PopUp[]>("SELECT * FROM popups " + $"where id = '{id}' ;", DataBase.ReturnTypeValue.PopUpValue);
            // Checks if popups has a length.
            // When popups has a length the message exists and the boolean will be set to true.
            bool returnValue = false;
            if (popups != null ){
                returnValue = popups.Length > 0;
            }
            return returnValue;
        }
    }
}
