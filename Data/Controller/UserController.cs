using Microsoft.Extensions.Configuration;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.DataBaseConnection;

namespace Nep_Cloud_Storage.Controller
{
    public class UserController : Controller<User>
    {
        public int? UserId{get; protected set;}
        public string UserRole{get; protected set;}

        /// <summary>
        /// Cannot send data to the database
        /// Use UserController(IConfiguration configuration) if you want to send data to database
        /// </summary>
        public UserController() : base(){}

        /// <summary>
        /// Configuration is needed if you want to send data to the database
        /// </summary>
        /// <param name="configuration">expects configuration</param>
        public UserController(IConfiguration configuration) : base (configuration) {}

        /// <summary>
        /// Creates a new user using already set properties and saves it to the database
        /// </summary>
        public override void Create()
        {
            User user = new User {Name = CName, Password = CPassword, Role = CUserRole, CompanyCode = CCompanyCode};
            Save(user);
        }

        /// <summary>
        /// Sets given parameters to be able to creat a new user.
        /// </summary>
        /// <param name="name">expects a name for the user</param>
        /// <param name="password">expects a password for the user</param>
        /// <param name="role">expects an UserRoles that indicates what type of user it is</param>
        /// <param name="companyCode">expects a companyCode for the user</param>
        /// <param name="create">expects a boolean, if the user needs to be created</param>
        public void SetAndCreate(string name, string password, UserRoles role, string companyCode, bool create)
        {
            CName = name; 
            CPassword = password; 
            CUserRole = role; 
            CCompanyCode = companyCode;
            // Creates new user when 'create' is true
            if(create)
            {
                Create();
            }
        }

        /// <summary>
        /// Saves a given user to the database
        /// </summary>
        /// <param name="user">expects an user</param>
        public virtual void Save(User user)
        {
            // Checks if name or password are empty
            // Sets name and password when true
            if(CName == null || CPassword == null)
            {
                CName = user.Name;
                CPassword = user.Password;
            }
            
            // Checks if the given user already exists in the database
            // If the user does not exist, saves the user to the database
            if (!ChecksAccountExist(user))
            {
                // Saves user to DB
                database.SendSQLCode($"CALL insert_user ('{CName}', '{CPassword}', '{user.Role}', '{user.CompanyCode}');");
                // Gets full user
                GetinProperty<User>(false);
                user = CUser;
                // Saves the maturitylevel of the user to DB
                database.SendSQLCode($"INSERT INTO `maturitylevel` (`userId`) VALUES ('{user.Id}');");
            }
        }

        /// <summary>
        /// Checks if the given user exists
        /// </summary>
        /// <param name="user">expects an user</param>
        public override bool ChecksAccountExist<User>(User user)
        {
            // Puts values of the user in the properties of the UserController
            dynamic d = user;
            CName = d.Name;
            CPassword = d.Password;

            // Gets full user from the database, using the set properties
            GetinProperty<User>(false);

            bool result = false;
            // 'result' is true, when 'CUser' is not empty
            if (CUser != null)
            {
                result = !CUser.isEmpty();
            }
            return result;
        }

        /// <summary>
        /// Gets an user and puts it in 'CUser' 
        /// Need to set certain variables before calling this method
        /// set: the id (CId), when onlyNameOrId is true
        /// set: the name (CName) and password (Cpassword), when onlyNameOrId is false
        /// </summary>
        /// <param name="onlyNameOrId">expects boolean to check if your only giving the name/id or if there are more parameters</param>
        public override void GetinProperty<User>(bool onlyNameOrId)
        {
            // List<User> users = (from user where id) 
            Model.User result;

            // Checks if the user should be picked with the id or with the name and password.
            if(onlyNameOrId)
            {
                result = database.SendSQLCodeWithReturn<Model.User>(
                "SELECT * FROM user " +
                $"where id = {CId};", DataBase.ReturnTypeValue.UserValue);
            }
            else
            {
                result = database.SendSQLCodeWithReturn<Model.User>($"call check_user ('{CName}', '{CPassword}'); ", DataBase.ReturnTypeValue.UserValue);
            }
            CUser = result;
        
            // Sets properties, when 'CUser' is not empty
            if (CUser != null)
            {
                UserId = CUser.Id;
                UserRole = (CUser.Role).ToString();
            }
        }

        /// <summary>
        /// Checks if the login is an existing user or admin.
        /// </summary>
        /// <param name="name">expects an username</param>
        /// <param name="password">expects a password of the user </param>
        public override void Login<User>(string name, string password)
        {
            CName = name;
            CPassword = password;

            // Creates an user from the loginForm
            Model.User CUser = new Model.User {Name = CName, Password = CPassword};

            // Checks if the user exists
            if(ChecksAccountExist<Model.User>(CUser))
            {
                GetinProperty<Model.User>(false);
                // User is found
                LoginNameFound = LoginType.TypeUser;
            }
        }
    }
}