using System;
using Microsoft.Extensions.Configuration;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.DataBaseConnection;
using Nep_Cloud_Storage.Interface;

namespace Nep_Cloud_Storage.Controller
{
    public abstract class Controller<T> : ControllerInterface<T> where T : class {
        [Flags]
        public enum LoginType
        {
            TypeCompany = 1,
            TypeUser = 2
        }   
        protected DataBase database;
        public LoginType LoginNameFound{get; protected set;}
        public int? CId{get; set;}
        public string CName{get; set;}
        public string CPassword{get; set;}
        public string CCompanyCode{get; set;}
        public UserRoles CUserRole{get; set;}
        public User CUser{get; set;}
        public Company CCompany{get; set;}

        /// <summary>
        /// Configuration is needed if you want to send to the database
        /// </summary>
        /// <param name="configuration">expects configuration</param>
        public Controller(IConfiguration configuration)
        {
            database = new DataBase(configuration);
        }

        /// <summary>
        /// Cannot send to the database
        /// Use Controller(IConfiguration configuration) if you want to send to database
        /// </summary>
        public Controller() {}

        /// <summary>
        /// Creates a new object using already set properties and saves it to the database
        /// </summary>
        public abstract void Create();

        /// <summary>
        /// Checks if the login is valid.
        /// </summary>
        /// <param name="name">expects a name of object T</param>
        /// <param name="password">expects a password of object T</param>
        public abstract void Login<T>(string name, string password);

        /// <summary>
        /// Checks if the a given object already exists
        /// </summary>
        /// <param name="t">expects possible object</param>
        public abstract bool ChecksAccountExist<T>(T t);
        
        /// <summary>
        /// gives a company or user
        /// Need to set certain variables before calling this method
        /// </summary>
        /// <param name="onlyNameOrId">expects boolean for if your only giving the name/id or if there are more parameters</param>
        public abstract void GetinProperty<T>(bool onlyNameOrId);
    }
}