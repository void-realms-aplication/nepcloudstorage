using Microsoft.Extensions.Configuration;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.DataBaseConnection;

namespace Nep_Cloud_Storage.Controller
{
    public class CompanyController : Controller<Company>
    {
        public string CompanyCodeName{get; protected set;}

        /// <summary>
        /// Cannot send data to the database
        /// Use CompanyController(IConfiguration configuration) if you want to send data to database
        /// </summary>
        public CompanyController() : base() {}

        /// <summary>
        /// Configuration is needed if you want to send to data the database
        /// </summary>
        /// <param name="configuration">expects configuration</param>
        public CompanyController(IConfiguration configuration) : base (configuration){}

        /// <summary>
        /// Creates a new company using already set properties and saves it to the database
        /// </summary>
        public override void Create()
        {
            Company company = new Company {CompanyCode = CCompanyCode, Password = CPassword};
            Save(company);
        }
        
        /// <summary>
        /// Sets given parameters to be able to creat a new user.
        /// </summary>
        /// <param name="companyCode">expects a code for the company</param>
        /// <param name="password">expects a password for the company</param>
        /// <param name="create">expects a boolean, if the company needs to be created</param>
        public void SetAndCreate(string companyCode, string password, bool create)
        {
            CCompanyCode = companyCode;
            CPassword = password;
            // Creates new company when 'create' is true
            if(create)
            {
                Create();
            }
        }

        /// <summary>
        /// Saves a given company to the database
        /// </summary>
        /// <param name="company">expects company</param>
        public virtual void Save(Company company)
        {
            // Checks if the given company already exists in the database
            // If the company does not exist, saves the company to the database
            if (!ChecksAccountExist<Company>(company))
            {
                database.SendSQLCode($"CALL insert_company ('{company.CompanyCode}', '{company.Password}');");
            }
        }

        /// <summary>
        /// Checks if the a given company already exists
        /// </summary>
        /// <param name="company">expects company</param>
        public override bool ChecksAccountExist<Company>(Company company)
        {
            // Puts values of the user in the properties of the CompanyController
            dynamic d = company;
            CCompanyCode = d.CompanyCode;
            CPassword = d.Password;

            // Gets company from the database, using the set properties
            GetinProperty<Company>(false);

            bool result = false;
            // 'result' is true, when 'CCompany' is not empty
            if (CCompany != null)
            {
                result = !CCompany.isEmpty();
            }
            return result;
        }

        /// <summary>
        /// Gets a company and puts it in 'CCompany' 
        /// Need to set certain variables before calling this method
        /// set: the companyCode (CCompanyCode), when onlyNameOrId is true
        /// set: the companyCode (CCompanyCode) and password (CPassword), when onlyNameOrId is false
        /// </summary>
        /// <param name="onlyNameOrId">expects boolean to check if your only giving the name/id or if there are more parameters</param>
        public override void GetinProperty<Company>(bool onlyNameOrId)
        {
            Model.Company result;

            // Checks if the company should be picked with only the name or also with the password.
            if (onlyNameOrId)
            {
                result = database.SendSQLCodeWithReturn<Model.Company>(
                "SELECT * FROM company " +
                $"where companyCode = '{CCompanyCode}';",DataBase.ReturnTypeValue.CompanyValue);
            } 
            else
            {
                result = database.SendSQLCodeWithReturn<Model.Company>($"call check_company ('{CCompanyCode}', '{CPassword}'); ", DataBase.ReturnTypeValue.CompanyValue);
            }
            CCompany = result;
        }

        /// <summary>
        /// Checks if the login is an existing company.
        /// </summary>
        /// <param name="name">expects a name of the company</param>
        /// <param name="password">expects a password of the company</param>
        public override void Login<Company>(string name, string password)
        {
            CCompanyCode = name;
            CPassword = password;

            // Creates company from the login values
            Model.Company company = new Model.Company {CompanyCode = CCompanyCode, Password = CPassword};

            //Checks if the company exists.
            if(ChecksAccountExist<Model.Company>(company))
            {
                CompanyCodeName = company.CompanyCode;
                // Company is found
                LoginNameFound = LoginType.TypeCompany;
            }
        }
    }
}