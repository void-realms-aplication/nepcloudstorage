using System;
using Microsoft.Extensions.Configuration;
using Nep_Cloud_Storage.DataBaseConnection;
using Nep_Cloud_Storage.Model;
using System.Linq;

namespace Nep_Cloud_Storage.Controller
{
    public class SessionController
    {
        TokenController tokenController;
        DataBase dataBase;
        DataBaseAccess dataBaseAccess;
        private int aFKTimer = 45;
        public SessionController(IConfiguration configuration)
        {
            tokenController = new TokenController();
            dataBase = new DataBase(configuration);
            dataBaseAccess = dataBase.sendDataBaseAccess();
        }

        /// <summary>
        /// creates sessionDate
        /// sessionKey already exists
        /// </summary>
        /// <param name="sessionKey">expects sessionKey</param>
        /// <param name="forAnUser">expects a bool that indicates if it is for an user</param>
        /// <param name="userId">expects an userId or null</param>
        /// <param name="companyCode">expects a companyCode or null</param>
        public SessionData CreateSession(string sessionKey, bool forAnUser, int? userId, string companyCode)
        {
            DateTime time = TimeNow();

            SessionData sessionData = new SessionData();
            sessionData.SessionKey = sessionKey;
            sessionData.ForAnUser = forAnUser;
            sessionData.UserId = userId;
            sessionData.CompanyCode = companyCode;
            sessionData.DataTimeStartSession = time;

            SessionToDB(sessionData);

            return sessionData;
        }

        /// <summary>
        /// creates sessionDate
        /// sessionKey will be generated
        /// </summary>
        /// <param name="name">expects the name of an user or company</param>
        /// <param name="password">expects the password of an user or company</param>
        /// <param name="forAnUser">expects a bool that indicates if it is for an user</param>
        /// <param name="userId">expects an userId or null</param>
        /// <param name="companyCode">expects a companyCode or null</param>
        public SessionData CreateSession(string name, string password, bool forAnUser, int? userId, string companyCode)
        {
            DateTime time = TimeNow();

            SessionData sessionData = new SessionData();
            sessionData.SessionKey = tokenController.CreateHash(name + password + time.AddMinutes(aFKTimer).ToString());
            sessionData.ForAnUser = forAnUser;
            sessionData.UserId = userId;
            sessionData.CompanyCode = companyCode;
            sessionData.DataTimeStartSession = time;

            SessionToDB(sessionData);

            return sessionData;
        }

        /// <summary>
        /// updates sessionDate
        /// creates new sessionData from the old one by resetting the timer and generating a new sessionKey
        /// places new sessionData in the database and also returns this sessionData
        /// </summary>
        /// <param name="sessionData">expects sessionData</param>
        /// <param name="name">expects the name of an user or company</param>
        /// <param name="password">expects the password of an user or company</param>
        public SessionData UpdateSession(SessionData sessionData, string name, string password)
        {
            DateTime time = TimeNow();

            string oldSessionKey = sessionData.SessionKey;
            // generates new sessionKey
            sessionData.SessionKey = tokenController.CreateHash(name + password + time.AddMinutes(aFKTimer).ToString()); 
            sessionData.DataTimeStartSession = time;

            // places new sessionKey and time in the database
            UpdateSessionToDB(sessionData, oldSessionKey); 

            return sessionData;
        }

        /// <summary>
        /// creates sessionDate
        /// sessionKey wil be generated
        /// </summary>
        /// <param name="sessionData">expects sessionData</param>
        /// <param name="name">expects the name of an user or the companyCode of a company</param>
        /// <param name="password">expects a password from the user or company</param>
        public bool CheckSession(SessionData sessionData, string name, string password)
        {
            return tokenController.CheckToken(sessionData.SessionKey, name, password, TimeNow(), aFKTimer);
        }

        /// <summary>
        /// gives the time it is now with Minute precision
        /// </summary>
        private DateTime TimeNow()
        {
            DateTime time = DateTime.Now;
            time = new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, 0);
            return time;
        }

        /// <summary>
        /// saves the session data to the database
        /// </summary>
        /// <param name="sessionData">expects a sessionData</param>
        private void SessionToDB(SessionData sessionData)
        {
            dataBase.SendSQLCode(
                $"INSERT INTO `session` (`sessionKey`, `forAnUser`, `dateTimeEndSession`, {(sessionData.ForAnUser ? "`userId`" : "`companyCode`")}) " +
                $"VALUES ('{sessionData.SessionKey}', {sessionData.ForAnUser}, '{sessionData.DataTimeStartSession.AddMinutes(aFKTimer).ToString("yyyy-MM-dd HH:mm:ss")}', {(sessionData.ForAnUser ? $"{sessionData.UserId}" : $"'{sessionData.CompanyCode}'")} );"
            );
        }

        /// <summary>
        /// updates the session data to the database
        /// </summary>
        /// <param name="newSessionData">expects newSessionData</param>
        /// <param name="oldSessionData">expects oldSessionData</param>
        private void UpdateSessionToDB(SessionData sessionData, string oldSessionKey)
        {
            dataBase.SendSQLCode(
                "UPDATE `session` " +
                $"SET `sessionKey` = '{sessionData.SessionKey}', `dateTimeEndSession` = '{sessionData.DataTimeStartSession.AddMinutes(aFKTimer).ToString("yyyy-MM-dd HH:mm:ss")}' " +
                $"WHERE (`sessionKey` = '{oldSessionKey}');"
            );
        }

        /// <summary>
        /// gets session data for a company or user from the database
        /// </summary>
        /// <param name="companyCode">expects a companyCode</param>
        public SessionData GetSession(SessionData sessionData)
        {
            // sessionData.CompanyCode;
            // sessionData.UserId;
            // sessionData.ForAnUser
            SessionData[] sessionsData = dataBase.SendSQLCodeWithSessionReturn(
                $"SELECT sessionKey, dateTimeEndSession, {(sessionData.ForAnUser ? "userId" : "companyCode")} FROM session " +
                $"where forAnUser = {(sessionData.ForAnUser ? 1 : 0)} and {(sessionData.ForAnUser ? $"userId like '{sessionData.UserId}'" : $"companyCode like '{sessionData.CompanyCode}'")} " +
                "order by dateTimeEndSession desc;", sessionData.ForAnUser);
            // in case sessionsData is an empty array, then it sends back an empty sessionData
            return (from SessionData SD in sessionsData
            orderby SD.DataTimeStartSession
            select SD).FirstOrDefault();
        }

        /// <summary>
        /// removes a session from the database
        /// </summary>
        /// <param name="sessionToken">expects a sessionToken</param>
        public void RemoveSession(string sessionToken)
        {
            dataBase.SendSQLCode($"DELETE FROM `session` WHERE (`sessionKey` = '{sessionToken}');");
        }
    }
}