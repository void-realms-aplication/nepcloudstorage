using System;
using System.Linq;
using System.Collections.Generic;
using Nep_Cloud_Storage.Data;
using System.Security.Cryptography;
using System.Text;

namespace Nep_Cloud_Storage.Controller
{
    public class TokenController
    {
        /// <summary>
        /// Creates a token from a string
        /// </summary>
        /// <param name="hashMe">expects a string to hash</param>
        public string CreateHash(string hashMe)
        {
            SHA256 sHA256 = SHA256.Create();
            byte[] hasMeHashed = sHA256.ComputeHash(Encoding.UTF8.GetBytes(hashMe));
            return BitConverter.ToString(hasMeHashed).Replace("-", "");
        }

        /// <summary>
        /// Checks if a given token is valid
        /// </summary>
        /// <param name="token">expects a token</param>
        /// <param name="name">expects a name</param>
        /// <param name="Password">expects a Password</param>
        /// <param name="DateTime">expects a the datetime now without the seconds (seconds on 0)</param>
        public bool CheckToken(string token, string name, string password, DateTime time, int aFKTimer)
        {
            // It is unknown what time is used to generate 'token'.
            // Covers all needed posssible time points to see if 'token'is valid.
            List<string> tokens = new List<string>();
            for (int i = 1; i <= aFKTimer; i++)
            {
                tokens.Add(CreateHash(name + password + time.AddMinutes(i).ToString()));
            }
            return tokens.FirstOrDefault(tokenToCheck => tokenToCheck.Contains(token)).Any();
        }
    }
}