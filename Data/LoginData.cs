namespace Nep_Cloud_Storage.Data
{
    public class LoginData
    {
        protected string oldPassword_;
        public string Password{ get; protected set;}
        public string Username{ get; protected set;}

        /// <summary>
        /// LoginData object can be created
        /// </summary>
        public LoginData()
        {
            //setup
            Password = "oldPassword";//default value. Needs to be changed in the future.
        }
        
        /// <summary>
        /// Changes value of existing password
        /// </summary>
        public void ChangePassword(string newPassword, string repeatedNewPassword)
        {
            //checks if the new password is the same as the existing password
            if(newPassword.Equals(Password))
            {
                //Todo: show error password already exists
            }
                //checks if the new password has any type errors
            else if (newPassword.Equals(repeatedNewPassword))
            {
                oldPassword_ = Password;
                Password = newPassword;
            } 
            else
            {
                //Todo: show error not the same password
            }
        }

        /// <summary>
        /// Changes value of existing username
        /// </summary>
        public void ChangeUsername(string newUsername)
        {
            Username = newUsername;
        }
        
    }
}