using System;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.Controller;
using System.Collections.Generic;

namespace Nep_Cloud_Storage.Test.Stubs
{
   public class CompanyControllerStub : CompanyController
   {
        // The stubs were used to test an older version of code.
        // The stubs need to be rewritten.
      /*  public bool Saved {get; set;}
        public Company CompanyCreated {get; set;}

        public CompanyControllerStub() : base() {}
        
        public override void Create(string companyCode, string password)
        {
            CompanyCreated = new Company {CompanyCode = companyCode, Password = password};
        }

        public Company ReturnCompany()
        {
            return CompanyCreated;
        }

        public override void Save(Company company)
        {
           if (CheckCompany(company))
           {
                Saved = true;
           }
        }

        public override Company[] TakeCompany(Company company)
        {
           List<Company> companies = new List<Company>();
           companies.Add(company);
           return companies.ToArray();
        }

        public override void Login(string companyCode, string password)
        {
            // Creates company from the login values
            Company company = new Company {CompanyCode = companyCode, Password = password};

            //Checks if the company exists.
            if(CheckCompany(company))
            {
                CompanyCodeName = company.CompanyCode;
                // Company is found
                LoginNameFound = LoginType.TypeCompany;
            }
        }*/
   }
}
