using Nep_Cloud_Storage.Controller;
using Nep_Cloud_Storage.Model;

namespace Nep_Cloud_Storage.Test.Stubs
{
    public class ChatControllerStub : ChatController
    {
        public PopUp receivedPopUp;

        public PopUp PopUpTest;

        public ChatControllerStub() : base(){}

        public override void CreateMessage(int id, string text, int difficultylevel, string style, int timeOfArrival)
        {
            PopUpTest = new PopUp();
            PopUpTest.Id = id;
            PopUpTest.Text = text;
            PopUpTest.DifficultyLevel = difficultylevel;
            PopUpTest.Style = style;
            PopUpTest.TimeOfArrival = timeOfArrival;
            SaveMessage(PopUpTest);
        }

        public override void SaveMessage(PopUp popUp)
        {
            receivedPopUp = popUp;
        }

        public override bool CheckMessageExist(int id)
        {
            bool exists = false;
            switch(id)
            {
                case 1:
                    string[] popUps1 = {"popUp1", "popUp2", "popUp3"};
                    exists = popUps1.Length > 0;
                    break;
                case 2:
                    string[] popUps2 = {"It's a popUp", "And another popUp"};
                    exists = popUps2.Length > 0;
                    break;
            }
            return exists;
        }
    }
}