using System;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.Controller;
using System.Collections.Generic;

namespace Nep_Cloud_Storage.Test.Stubs
{
   public class UserControllerStub : UserController
   {
        // The stubs were used to test an older version of code.
        // The stubs need to be rewritten.
       /* public bool Saved {get; set;}
        public User UserCreated {get; set;}

        public UserControllerStub() : base() {}
        
        public override void Create(string name, string password, UserRoles role, string companyCode)
        {
            UserCreated = new User {Name = name, Password = password, Role = role, CompanyCode = companyCode};
        }

        public User ReturnUser()
        {
            return UserCreated;
        }

        public override void Save(User user)
        {
           if (CheckUser(user))
           {
                Saved = true;
           }
        }

        public override User[] TakeUser(User user)
        {
           List<User> users = new List<User>();
           users.Add(user);
           return users.ToArray();
        }

        public override void Login(string name, string password, UserRoles role)
        {
            User user = new User {Name = name, Password = password};

            if(CheckUser(user))
            {
                UserRole = role.ToString();
                UserId = user.Id;

                LoginNameFound = LoginType.TypeUser;
            }
        }*/
   }
}
