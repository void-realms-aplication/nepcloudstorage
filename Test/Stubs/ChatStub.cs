using System;
using Nep_Cloud_Storage.Data;
namespace Nep_Cloud_Storage.Test.Stubs
{
   public class ChatStub : Chat
   {
       public int CounterStub{get{return Counter;} set{Counter = value;}}
       public ChatStub() : base()
       {
            Times = new int[3];
            Times[0] = 0;
            Times[1] = 1;
            Times[2] = 3;
            Counter = 0;
            MessageSend = false;
       }
       public override void SendMessage()
       {
            switch(Times[Counter])
            {
               case 0:
                    Message = "hallo";
                    break;
               case 1:
                    Message = "Hey there";
                    break;               
               case 2:
                    Message = "hola";
                    break;
               case 3:
                    Message = "konnichiwa";
                    break;
            }
            
            MessageSend = true;
       }
   }
}
