using Xunit;
using Nep_Cloud_Storage.Controller;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.Test.Stubs;

namespace NepCloudStorageTests
{
    public class ChatControllerTest
    {
        ChatControllerStub chatControllerStub = new ChatControllerStub();

        //TO DO: Fix the error on line 38. This is the same as occurs in the ChatTest.
        [Fact]
        public void CreateMessage_Create()
        {
            //arrange
            int id = 1;
            string text = "example";
            int difficultyLevel = 2;
            string style = "virus";
            int timeOfArrival = 3;

            PopUp expected = new PopUp();
            expected.Id = id;
            expected.Text = text;
            expected.DifficultyLevel = difficultyLevel;
            expected.Style = style;
            expected.TimeOfArrival = timeOfArrival;

            bool sameId;
            bool sameText;
            bool sameDifficultyLevel;
            bool sameStyle;
            bool sameTimeOfArrival;
            bool messageCreated;

            //act
            chatControllerStub.CreateMessage(id, text, difficultyLevel, style, timeOfArrival);

            sameId = expected.Id.Equals(chatControllerStub.receivedPopUp.Id);
            sameText = expected.Text.Equals(chatControllerStub.receivedPopUp.Text);
            sameDifficultyLevel = expected.DifficultyLevel.Equals(chatControllerStub.receivedPopUp.DifficultyLevel);
            sameStyle = expected.Style.Equals(chatControllerStub.receivedPopUp.Style);
            sameTimeOfArrival = expected.TimeOfArrival.Equals(chatControllerStub.receivedPopUp.TimeOfArrival);
            messageCreated = sameId && sameText && sameDifficultyLevel && sameStyle && sameTimeOfArrival;
            
            //assert
            Assert.True(messageCreated);
        }

        [Fact]
        public void CheckMessageExist_MessageExists()
        {
            //arrange
            int id = 1;
            bool result;

            //act
            result = chatControllerStub.CheckMessageExist(id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void CheckMessageExist_MessageDoesNotExist()
        {
            //arrange
            int id = 3;
            bool result;

            //act
            result = chatControllerStub.CheckMessageExist(id);

            //assert
            Assert.False(result);
        }
    }
}