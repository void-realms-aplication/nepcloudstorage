using Xunit;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.Test.Stubs;
using System.Collections.Generic;

    
namespace NepCloudStorageTests
{
    public class UserControllerTest
    {
        // The tests tested an older version of the code.
        // The tests need to be rewritten.
        /*UserControllerStub userControllerStub = new UserControllerStub();
        
        [Fact]
        public void Create_UserIsCreated()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.User;
            string companyCode = "bedrijf1";
            User expectedUser = new User {Name = name, Password = password, Role = role, CompanyCode = companyCode};
            bool sameName;
            bool samePassword; 
            bool sameRole;
            bool sameCompanyCode;
            bool sameUsers;
            
            //act
            userControllerStub.Create(name, password, role, companyCode);
            sameName = expectedUser.Name.Equals(userControllerStub.UserCreated.Name);
            samePassword = expectedUser.Password.Equals(userControllerStub.UserCreated.Password);
            sameRole = expectedUser.Role.Equals(userControllerStub.UserCreated.Role);
            sameCompanyCode = expectedUser.CompanyCode.Equals(userControllerStub.UserCreated.CompanyCode);
            sameUsers = sameName && samePassword && sameRole && sameCompanyCode;

            //assert
            Assert.True(sameUsers);
        }

        [Fact]
        public void Create_AdminIsCreated()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.Admin;
            string companyCode = "bedrijf1";
            User expectedUser = new User {Name = name, Password = password, Role = role, CompanyCode = companyCode};
            bool sameName;
            bool samePassword; 
            bool sameRole;
            bool sameCompanyCode;
            bool sameUsers;
            
            //act
            userControllerStub.Create(name, password, role, companyCode);
            sameName = expectedUser.Name.Equals(userControllerStub.UserCreated.Name);
            samePassword = expectedUser.Password.Equals(userControllerStub.UserCreated.Password);
            sameRole = expectedUser.Role.Equals(userControllerStub.UserCreated.Role);
            sameCompanyCode = expectedUser.CompanyCode.Equals(userControllerStub.UserCreated.CompanyCode);
            sameUsers = sameName && samePassword && sameRole && sameCompanyCode;

            //assert
            Assert.True(sameUsers);
        }

        [Fact]
        public void Save_UserIsSaved()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.User;
            string companyCode = "bedrijf1";
            User user = new User{Name = name, Password = password, Role = role, CompanyCode = companyCode};
            userControllerStub.Saved = false;
            
            //act
            userControllerStub.Save(user);
           
            //assert
            Assert.True(userControllerStub.Saved);
        }
        [Fact]
        public void Save_AdminIsSaved()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.Admin;
            string companyCode = "bedrijf1";
            User user = new User{Name = name, Password = password, Role = role, CompanyCode = companyCode};
            userControllerStub.Saved = false;
            
            //act
            userControllerStub.Save(user);
           
            //assert
            Assert.True(userControllerStub.Saved);
        }

        [Fact]
        public void TakeUser_ReturnsUser()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.User;
            string companyCode = "bedrijf1";
            User user = new User{Name = name, Password = password, Role = role, CompanyCode = companyCode};
            User[] actualUsers;
            User[] expectedUsers;
            List<User> users = new List<User>();
            
            //act
            users.Add(user);
            expectedUsers = users.ToArray();
            actualUsers = userControllerStub.TakeUser(user);
           
            //assert
            Assert.Equal(expectedUsers, actualUsers);
        }

        [Fact]
        public void CheckUser_UserExists()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.User;
            string companyCode = "bedrijf1";
            User user = new User {Name = name, Password = password, Role = role, CompanyCode = companyCode};
            bool actual;
            
            //act
            actual = userControllerStub.CheckUser(user);
           
            //assert
            Assert.True(actual);
        }

        [Fact]
        public void Login_loginNameFoundIsTypeUser()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.User;
            UserControllerStub.LoginType expected = UserControllerStub.LoginType.TypeUser;
            UserControllerStub.LoginType actual;

            //act
            userControllerStub.Login(name, password, role);
            actual = userControllerStub.LoginNameFound;
           
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Login_UserRoleIsUser()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.User;
            string expected = "User";
            string actual;
            
            //act
            userControllerStub.Login(name, password, role);
            actual = userControllerStub.UserRole;
           
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Login_UserRoleIsAdmin()
        {
            //arrange
            string name = "gebruiker1";
            string password = "wachtwoord";
            UserRoles role = UserRoles.Admin;
            string expected = "Admin";
            string actual;
            
            //act
            userControllerStub.Login(name, password, role);
            actual = userControllerStub.UserRole;
           
            //assert
            Assert.Equal(expected, actual);
        }*/
    }
}