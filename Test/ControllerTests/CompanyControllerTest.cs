using Xunit;
using Nep_Cloud_Storage.Model;
using Nep_Cloud_Storage.Test.Stubs;
using System.Collections.Generic;

    
namespace NepCloudStorageTests
{
    public class CompanyControllerTest
    {
        // The tests tested an older version of the code.
        // The tests need to be rewritten.
       /* CompanyControllerStub companyControllerStub = new CompanyControllerStub();
        
        [Fact]
        public void Create_CompanyIsCreated()
        {
            //arrange
            string companyCode = "bedrijf1";
            string password = "wachtwoord";
            Company expectedCompany = new Company {CompanyCode = companyCode, Password = password};
            bool sameId;
            bool samePassword; 
            
            //act
            companyControllerStub.Create(companyCode, password);
            sameId = expectedCompany.CompanyCode.Equals(companyControllerStub.CompanyCreated.CompanyCode);
            samePassword = expectedCompany.Password.Equals(companyControllerStub.CompanyCreated.Password);

            //assert
            Assert.Equal(sameId, samePassword);
        }

        [Fact]
        public void Save_CompanyIsSaved()
        {
            //arrange
            string companyCode = "bedrijf1";
            string password = "wachtwoord";
            Company company = new Company {CompanyCode = companyCode, Password = password};
            companyControllerStub.Saved = false;
            
            //act
            companyControllerStub.Save(company);
           
            //assert
            Assert.True(companyControllerStub.Saved);
        }

        [Fact]
        public void TakeCompany_ReturnsCompany()
        {
            //arrange
            string companyCode = "bedrijf1";
            string password = "wachtwoord";
            Company company = new Company {CompanyCode = companyCode, Password = password};
            Company[] actualCompanies;
            Company[] expectedCompanies;
            List<Company> companies = new List<Company>();
            
            //act
            companies.Add(company);
            expectedCompanies = companies.ToArray();
            actualCompanies = companyControllerStub.TakeCompany(company);
           
            //assert
            Assert.Equal(expectedCompanies, actualCompanies);
        }

        [Fact]
        public void CheckCompany_CompanyExists()
        {
            //arrange
            string companyCode = "bedrijf1";
            string password = "wachtwoord";
            Company company = new Company {CompanyCode = companyCode, Password = password};
            bool actual;
            
            //act
            actual = companyControllerStub.CheckCompany(company);
           
            //assert
            Assert.True(actual);
        }

        [Fact]
        public void Login_loginNameFoundIsTypeCompany()
        {
            //arrange
            string companyCode = "bedrijf1";
            string password = "wachtwoord1";
            CompanyControllerStub.LoginType expected = CompanyControllerStub.LoginType.TypeCompany;
            CompanyControllerStub.LoginType actual;
            
            //act
            companyControllerStub.Login(companyCode, password);
            actual = companyControllerStub.LoginNameFound;
           
            //assert
            Assert.Equal(expected, actual);
        }*/
    }
}