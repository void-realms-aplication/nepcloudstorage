using Xunit;
using Nep_Cloud_Storage.Controller;
using System;

namespace NepCloudStorageTests
{
    public class TokenControllerTest
    {
        TokenController TokenController = new TokenController();

        [Fact]
        public void CreateHash_TokenCreated()
        {
            //arrange
            string name = "TestUser";
            string password = "E7CF3EF4F17C3999A94F2C6F612E8A888E5B1026878E4E19398B23BD38EC221A";
            DateTime time = new DateTime(2021, 1, 31, 11, 13, 0);
            int afkTimer = 45;
            string tokenData = name + password + time.AddMinutes(afkTimer).ToString();
            string expected = "037C3525B584201CC16F3BEB5511F94D6C6637AA287434565D5E6F84FA869732";
            string actual;
            
            //act
            actual = TokenController.CreateHash(tokenData);
            
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CheckToken_TokenExists()
        {
            //arrange
            string token = "037C3525B584201CC16F3BEB5511F94D6C6637AA287434565D5E6F84FA869732";
            string name = "TestUser";
            string password = "E7CF3EF4F17C3999A94F2C6F612E8A888E5B1026878E4E19398B23BD38EC221A";
            DateTime time = new DateTime(2021, 1, 31, 11, 13, 0);
            int afkTimer = 45;
            bool actual;
            
            //act
            actual = TokenController.CheckToken(token, name, password, time, afkTimer);
            
            //assert
            Assert.True(actual);
        }

        [Fact]
        public void Check_TokenDoesNotExist()
        {
            //arrange
            string token = "037C3525B584201CC16F3BEB5511F94D6C6637AA287434565D5E6F84FA869732";
            string name = "TestUser";
            string password = "E7CF3EF4F17C3999A94F2C6F612E8A888E5B1026878E4E19398B23BD38EC221A";
            DateTime time = new DateTime(2021, 1, 30, 10, 5, 0);
            int afkTimer = 45;
            bool actual;
            
            //act
            actual = TokenController.CheckToken(token, name, password, time, afkTimer);
            
            //assert
            Assert.False(actual);
        }

        [Fact]
        public void CreateHash_PasswordHasBeenHashed()
        {
            //arrange
            string password = "5678";
            string expected = "F8638B979B2F4F793DDB6DBD197E0EE25A7A6EA32B0AE22F5E3C5D119D839E75";
            string result;

            //act
            result =  TokenController.CreateHash(password);

            //assert
            Assert.Equal(result, expected);
        }
    }
}