using System;
using Xunit;
using Nep_Cloud_Storage.Data;
using Nep_Cloud_Storage.Test.Stubs;
namespace NepCloudStorageTests
{
    public class ChatTest
    {
        ChatStub chatStub = new ChatStub();
        
        [Fact]
        public void SendMessage_FirstMessage()
        {
            //arrange
            string expected = "hallo";
            //act
            chatStub.SendMessage();

            //assert
            Assert.Equal(chatStub.Message, expected);
        }

        [Fact]
        public void SendMessage_SecondMessage()
        {
            //arrange
            string expected = "Hey there";
            chatStub.CounterStub = 1;
            //act
            chatStub.SendMessage();

            //assert
            Assert.Equal(chatStub.Message, expected);
        }

        [Fact]
        public void SetTimer_AlarmTimeIsSet_TimeToSendIsZero()
        {
            //arrange
            int expected = 1;
            
            //act
            chatStub.SetTimer();

            //assert
            Assert.Equal(chatStub.AlarmTimer, expected);
        }
        [Fact]
        public void SetTimer_AlarmTimeIsSet_TimeToSendIsMoreThanZero()
        {
            //arrange
            chatStub.Times[0] = 1;
            int expected = 60000;
            
            //act
            chatStub.SetTimer();

            //assert
            Assert.Equal(chatStub.AlarmTimer, expected);
        }
    }
}
