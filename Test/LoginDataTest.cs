using Xunit;
using Nep_Cloud_Storage.Data;

namespace NepCloudStorageTests
{
    public class LoginDataTest
    {
        LoginData loginData = new LoginData();
        
        [Fact]
        public void ChangePassword_PasswordIsChanged()
        {
            //arrange
            //Password needs to be set. Password has for now a default value: oldpassword.
            string newPassword = "newPassword";
            string repeatedNewPassword = newPassword;
            
            //act
            loginData.ChangePassword(newPassword, repeatedNewPassword);

            //assert
            Assert.Equal(loginData.Password, newPassword);
        }

        [Fact]
        public void ChangeUsername_UsernameIsChanged()
        {
            //arrange
            string newUsername = "newUsername";

            //act
            loginData.ChangeUsername(newUsername);

            //assert
            Assert.Equal(loginData.Username, newUsername);
        }
         
    }
}