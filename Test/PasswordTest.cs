using Xunit;
using Nep_Cloud_Storage.Data;

namespace NepCloudStorageTests
{
    public class PasswordTest
    {
        Password password = new Password();
        [Fact]
         public void CheckCharactersAmount_SpecialCharacterAdded()
        {
            //arrange
            string character = "!";
            
            //password.PasswordAmountOfSpecialCharacters = 0;
            int expected = 1;

            //act
            password.CheckCharactersAmount(character);

            //assert
            Assert.Equal(password.PasswordAmountOfSpecialCharacters, expected);
        }

        [Fact]
        public void CheckCharactersAmount_NumberAdded()
        {
            //arrange
            string character = "1";
            
            //password.PasswordAmountOfNumbers = 0;
            int expected = 1;

            //act
            password.CheckCharactersAmount(character);

            //assert
            Assert.Equal(password.PasswordAmountOfNumbers, expected);
        }

        [Fact]
        public void CheckCharactersAmount_UpperCaseAdded()
        {
            //arrange
            string  character = "B";
            
            //password.PasswordAmountUpperCase = 0;
            int expected = 1;

            //act
            password.CheckCharactersAmount(character);

            //assert
            Assert.Equal(password.PasswordAmountUpperCase, expected);
        }
         
        [Fact]
        public void CheckCharactersAmount_LowerCaseAdded()
        {
            //arrange
            string character = "b";
           
            //password.PasswordAmountLowerCase = 0;
            int expected = 1;

            //act
            password.CheckCharactersAmount(character);

            //assert
            Assert.Equal(password.PasswordAmountLowerCase, expected);
        }

       [Fact]
        public void CheckEntropyLevel_EntropyLevelLow()
        {
            //arrange
            Password p = new Password(4, 1, 0, 0, 3, 0);
            double expected = 20.67970000576925;

            //act
            p.CheckEntropyLevel();

            //assert
            Assert.Equal(p.PasswordEntropyLevel, expected);
        }

        [Fact]
        public void CheckEntropyLevel_EntropyLevelHigh()
        {
            //arrange
            Password p = new Password(12, 2, 2, 4, 4, 0);
            double expected = 78.65506622013164;

            //act
            p.CheckEntropyLevel();

            //assert
            Assert.Equal(p.PasswordEntropyLevel, expected);
        }

        [Fact]
        public void CheckPossibleAmountOfUniqueCharacters_SpecialCharactersUsed()
        {
            //arrange
            Password p = new Password(1, 0, 1, 0, 0, 0);
            int expected = 32;

            //act
            p.CheckPossibleAmountOfUniqueCharacters();

            //assert
            Assert.Equal(p.UniqueCharacters, expected);
        }
        
        [Fact]
        public void CheckPossibleAmountOfUniqueCharacters_NumberUsed()
        {
            //arrange
            Password p = new Password(1, 1, 0, 0, 0, 0);
            int expected = 10;

            //act
            p.CheckPossibleAmountOfUniqueCharacters();

            //assert
            Assert.Equal(p.UniqueCharacters, expected);
        }

        [Fact]
        public void CheckPossibleAmountOfUniqueCharacters_UpperCaseUsed()
        {
            //arrange
            Password p = new Password(1, 0, 0, 1, 0, 0);
            int expected = 26;

            //act
            p.CheckPossibleAmountOfUniqueCharacters();

            //assert
            Assert.Equal(p.UniqueCharacters, expected);
        }

        [Fact]
        public void CheckPossibleAmountOfUniqueCharacters_LowerCaseUsed()
        {
            //arrange
            Password p = new Password(0, 0, 0, 0, 1, 0);
            int expected = 26;

            //act
            p.CheckPossibleAmountOfUniqueCharacters();

            //assert
            Assert.Equal(p.UniqueCharacters, expected);
        }        
        
        [Fact]
        public void CheckPasswordLevelOfStrength_MaturityLevel1()
        {
        //arrange
        Password p = new Password(4, 1, 0, 0, 3, 20.67970000576925);
        int expected = 1;

        //act
        password.CheckPasswordLevelOfStrength();

        //assert
        Assert.Equal(password.PasswordStrengthLevel, expected);
        }
        
        [Fact]
        public void CheckPasswordLevelOfStrength_MaturityLevel2()
        {
        //arrange
        Password p = new Password(8, 1, 1, 2, 3, 52.4367108134211);
        int expected = 2;

        //act
        p.CheckPasswordLevelOfStrength();

        //assert
        Assert.Equal(p.PasswordStrengthLevel, expected);
        }
    
        [Fact]
        public void CheckPasswordLevelOfStrength_MaturityLevel3()
        {
        //arrange
        Password p = new Password(10, 1, 1, 2, 6, 65.54588851677637);
        int expected = 3;

        //act
        p.CheckPasswordLevelOfStrength();

        //assert
        Assert.Equal(p.PasswordStrengthLevel, expected);
        }

        [Fact]
        public void CheckPasswordLevelOfStrength_MaturityLevel4()
        {
        //arrange
        Password p = new Password(12, 1, 2, 3, 6, 78.65506622013164);
        int expected = 4;

        //act
        p.CheckPasswordLevelOfStrength();

        //assert
        Assert.Equal(p.PasswordStrengthLevel, expected);
        }

        [Fact]
        public void CheckPasswordLevelOfStrength_MaturityLevel5()
        {
        //arrange
        Password p = new Password(20, 6, 4, 4, 6, 131.09177703355275);
        int expected = 5;

        //act
        p.CheckPasswordLevelOfStrength();

        //assert
        Assert.Equal(p.PasswordStrengthLevel, expected);
        }
    
        [Fact]
        public void CheckPasswordStrength_PasswordLevel1()
        {
        //arrange
        string wordOfIdentification = "1234";

        int expected = 1;

        //act
        password.CheckPasswordStrength(wordOfIdentification);

        //assert
        Assert.Equal(password.PasswordStrengthLevel, expected);
        }

        [Fact]
        public void CheckPasswordStrength_PasswordLevel2()
        {
        //arrange
        string wordOfIdentification = "cookies1!";

        int expected = 2;

        //act
        password.CheckPasswordStrength(wordOfIdentification);

        //assert
        Assert.Equal(password.PasswordStrengthLevel, expected);
        }

        [Fact]
        public void CheckPasswordStrength_PasswordLevel3()
        {
        //arrange
        string wordOfIdentification = "Cookies12!";

        int expected = 3;

        //act
        password.CheckPasswordStrength(wordOfIdentification);

        //assert
        Assert.Equal(password.PasswordStrengthLevel, expected);
        }

        [Fact]
        public void CheckPasswordStrength_PasswordLevel4()
        {
        //arrange
        string wordOfIdentification = "Cookies1234!";

        int expected = 4;

        //act
        password.CheckPasswordStrength(wordOfIdentification);

        //assert
        Assert.Equal(password.PasswordStrengthLevel, expected);
        }

        [Fact]
        public void CheckPasswordStrength_PasswordLevel5()
        {
        //arrange
        string wordOfIdentification = "Cookies!AndCookies123!";

        int expected = 5;

        //act
        password.CheckPasswordStrength(wordOfIdentification);

        //assert
        Assert.Equal(password.PasswordStrengthLevel, expected);
        }
    }
}