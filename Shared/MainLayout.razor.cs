using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace Nep_Cloud_Storage.Shared
{
    public partial class MainLayout
    {
        protected override async Task OnInitializedAsync()
        {
            AuthenticationState authState = await AuthenticationStateProvider.GetAuthenticationStateAsync();
            if (!(authState.User.IsInRole("Admin") || authState.User.IsInRole("User") || authState.User.IsInRole("company")))
            {
            NavigationManager.NavigateTo("/Login");
            }
        }

        protected void LogoutButton()
        {
            NavigationManager.NavigateTo("/Logout");
        }
    }
}